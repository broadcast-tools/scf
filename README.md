The Subtitling Conversion Framework (SCF) is a set of modules for converting XML based subtitle formats. Main target is to build up a flexible and extensible transformation pipeline to convert EBU STL formats and EBU-TT subtitle formats.

SCF is provided and developed by the Institut fuer Rundfunktechnik.

More about it on GitHub: https://github.com/IRT-Open-Source/scf

This is only a build repository to provide an automatic build of the Docker container and a docker-compose file.